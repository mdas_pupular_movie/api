package org.acme.api.movies.cart.Domain.Saver;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CartSaver {
    @Inject
    InfinispanCartRepository cartRepository;

    public void save(Cart cart) throws CustomException {
        this.existCart(cart.cartId());
        cartRepository.update(cart);
    }

    public void existCart(CartId cartId) throws CustomException {
        if (!cartRepository.exists(cartId)){
            throw new CustomException("Cart not found.", 404);
        }
    }
}
