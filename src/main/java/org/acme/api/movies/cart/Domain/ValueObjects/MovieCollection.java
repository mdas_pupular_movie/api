package org.acme.api.movies.cart.Domain.ValueObjects;

import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.infinispan.protostream.annotations.ProtoFactory;
import org.infinispan.protostream.annotations.ProtoField;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class MovieCollection {

    private Collection<MovieCart> movies;

    @ProtoFactory
    public MovieCollection(Collection<MovieCart> movies) {
        this.movies = movies;
    }

    public MovieCollection() {
        this.movies = new ArrayList<>();
    }

    @ProtoField(number = 1)
    public Collection<MovieCart> movies() {
        return movies;
    }

    public void addMovie(MovieCart movie){
        if(this.movies.stream().anyMatch(x -> Objects.equals(x.movieId(), movie.movieId()))){
            this.movies.stream().filter(c -> movie.movieId().equals(c.movieId()))
                .forEach(m -> m.quantity().setQuantity(1));
            return;
        }
        this.movies.add(movie);
    }

    public void removeMovie(MovieId movieId){
        if(this.movies.stream().anyMatch(x -> Objects.equals(x.movieId(), movieId.movieId()))){
            AtomicBoolean flagConcurrent = new AtomicBoolean(false);
            this.movies.stream().filter(c -> movieId.movieId().equals(c.movieId()))
                .forEach(m -> {
                    if (m.quantity().getQuantity() > 1){
                        m.quantity().setQuantity(-1);
                        return;
                    }
                    flagConcurrent.set(true);
                });

            if(flagConcurrent.get()){
                this.movies.removeIf(c -> movieId.movieId().equals(c.movieId()));
            }
        }
    }

    public void removeAll(){
        this.movies = new ArrayList<>();
    }

    public void addAll(Collection<MovieCart> movies){
        this.movies.addAll(movies);
    }

    public ArrayList<MovieId> getMovieIds(){
        ArrayList<MovieId> result = new ArrayList<MovieId>();
        this.movies.forEach(movieCart -> result.add(new MovieId(movieCart.movieId())));
        return result;
    }
}
