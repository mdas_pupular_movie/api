package org.acme.api.movies.cart.Infraestructure.Repositories;

import io.quarkus.infinispan.client.Remote;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Interfaces.CartRepository;
import org.acme.api.movies.cart.Domain.Marshaller.CartMarshaller;
import org.acme.api.movies.cart.Domain.Marshaller.MovieCartMarshaller;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.protostream.MessageMarshaller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Produces;

@ApplicationScoped
public class InfinispanCartRepository implements CartRepository {

    @Produces
    MessageMarshaller cartMarshaller() {
      return new CartMarshaller();
   }

    @Produces
    MessageMarshaller movieCartMarshaller() {
      return new MovieCartMarshaller();
   }

    @Inject
    @Remote("cartCache")
    RemoteCache<String, Cart> cache;

    @Override
    public Cart getCart(String cartId) {
        return cache.get(cartId);
    }

    @Override
    public void save(Cart cart) {
        cache.putIfAbsent(cart.cartId().getId(), cart);
    }

    @Override
    public void update(Cart cart) {
        cache.replace(cart.cartId().getId(), cart);
    }

    @Override
    public boolean exists(CartId cartId) {
        return cache.get(cartId.getId()) != null;
    }

    @Override
    public void removeCart(CartId cartId) {
        cache.remove(cartId.getId());
    }
}
