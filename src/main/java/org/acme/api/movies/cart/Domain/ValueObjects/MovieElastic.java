package org.acme.api.movies.cart.Domain.ValueObjects;

import java.util.Objects;

public class MovieElastic {
    private String movieId;
    private String title;
    private Price price;
    private Quantity quantity;

    public MovieElastic(String movieId, String title, Price price, Quantity quantity) {
        this.movieId = movieId;
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public MovieElastic() {
    }

    public String movieId() {
        return movieId;
    }

    public String title() {
        return title;
    }

    public Price price() {
        return price;
    }

    public Quantity quantity() {
        return quantity;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieElastic)) {
            return false;
        }
        MovieElastic other = (MovieElastic) o;
        return Objects.equals(movieId, other.movieId)
                && Objects.equals(price.getPriceValue(), other.price.getPriceValue())
                && Objects.equals(title, other.title)
                && Objects.equals(quantity.getQuantity(), other.quantity.getQuantity());
    }
}
