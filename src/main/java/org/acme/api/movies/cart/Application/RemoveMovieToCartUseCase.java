package org.acme.api.movies.cart.Application;

import org.acme.api.movies.cart.Application.DTO.CartMovieDto;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Finder.CartFinder;
import org.acme.api.movies.cart.Domain.Finder.MovieCartFinder;
import org.acme.api.movies.cart.Domain.Saver.CartSaver;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieId;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class RemoveMovieToCartUseCase {
    @Inject
    CartFinder finder;

    @Inject
    CartSaver saver;

    @Inject
    MovieCartFinder movieCartFinder;

    public void execute(CartMovieDto cartMovieDto) throws IOException, CustomException {
        Cart cart = finder.find(new CartId(cartMovieDto.cartId()));
        MovieElastic movie = movieCartFinder.find(new MovieId(cartMovieDto.movieId()));
        cart.moviesList().removeMovie(new MovieId(movie.movieId()));
        saver.save(cart);
    }
}
