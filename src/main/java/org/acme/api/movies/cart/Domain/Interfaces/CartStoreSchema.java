package org.acme.api.movies.cart.Domain.Interfaces;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.cart.Domain.ValueObjects.Quantity;
import org.infinispan.protostream.GeneratedSchema;
import org.infinispan.protostream.annotations.AutoProtoSchemaBuilder;

@AutoProtoSchemaBuilder(includeClasses = {
        MovieCart.class,
        Quantity.class,
        CartId.class,
        MovieCollection.class,
        MovieCart.class,
        Cart.class
}, schemaPackageName = "cart_table")
public interface CartStoreSchema extends GeneratedSchema {
}
