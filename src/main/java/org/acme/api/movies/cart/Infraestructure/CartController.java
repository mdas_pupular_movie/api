package org.acme.api.movies.cart.Infraestructure;

import io.vertx.core.json.JsonObject;
import org.acme.api.movies.cart.Application.*;
import org.acme.api.movies.cart.Application.DTO.CartMovieDto;
import org.acme.api.movies.cart.Domain.Resources.CartResponseResource;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.ControllerRequest.CartMovieControllerRequest;
import org.acme.api.shared.Domain.CustomException;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/cart")
public class CartController {

    @Inject
    GetCartUseCase getCartUseCase;

    @Inject
    CreateCartUseCase createCartUseCase;

    @Inject
    RemoveCartUseCase removeCart;

    @Inject
    AddMovieToCartUseCase addMovieToCart;

    @Inject
    RemoveMovieToCartUseCase removeMovieToCart;

    @GET
    @Path("{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCart(@PathParam("cartId") String cartId) throws CustomException, IOException {
        CartResponseResource cart = getCartUseCase.execute(cartId);
        return Response.status(200).entity(JsonObject.mapFrom(cart)).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postCart() {
        CartId id = createCartUseCase.execute();
        return Response.status(200).entity(new JsonObject().put("cartId", id.getId())).build();
    }

    @DELETE
    @Path("{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeCart(@PathParam("cartId") String cartId) throws CustomException {
        removeCart.execute(new CartId(cartId));
        return Response.status(200).build();
    }

    @POST
    @Path("/addMovie")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMovieCart(CartMovieControllerRequest request) throws IOException, CustomException {
        addMovieToCart.execute(new CartMovieDto(
                request.cartId,
                request.movieId
        ));
        return Response.status(200).build();
    }

    @DELETE
    @Path("/removeMovie")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeMovieCart(CartMovieControllerRequest request) throws IOException, CustomException {
        removeMovieToCart.execute(new CartMovieDto(
                request.cartId,
                request.movieId
        ));
        return Response.status(200).build();
    }
}
