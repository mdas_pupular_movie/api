package org.acme.api.movies.movie.Domain.Entity;

import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;

import java.util.Objects;

public class Movies {
    private MovieCollection movies;
    private Pagination pagination;
    private int totalMovies;

    public Movies(MovieCollection movieCollection, Pagination pagination, int totalMovies) {
        this.movies = movieCollection;
        this.pagination = pagination;
        this.totalMovies = totalMovies;
    }

    public MovieCollection movies() {
        return movies;
    }

    public Pagination pagination() {
        return pagination;
    }

    public int totalMovies() {
        return totalMovies;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movies)) {
            return false;
        }
        Movies other = (Movies) o;
        return Objects.equals(movies, other.movies)
                && Objects.equals(pagination, other.pagination)
                && Objects.equals(totalMovies, other.totalMovies);
    }
}
