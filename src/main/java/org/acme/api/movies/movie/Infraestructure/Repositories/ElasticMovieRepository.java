package org.acme.api.movies.movie.Infraestructure.Repositories;

import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.Interfaces.MovieRepository;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;

import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import io.vertx.core.json.JsonObject;
import org.apache.http.util.EntityUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import io.vertx.core.json.JsonArray;

@ApplicationScoped
public class ElasticMovieRepository implements MovieRepository {

    @Inject
    RestClient restClient;

    @Override
    @Transactional
    public Movies getMovies(Pagination pagination) throws IOException {
        Request request = new Request(
                "GET",
                "/movies/_search");

        JsonObject matchJson = new JsonObject().put("match_all", new JsonObject());

        if (!Objects.equals(pagination.searchBy(), "") && !Objects.equals(pagination.searchTearm(), "")){
            matchJson = new JsonObject().put("wildcard", new JsonObject().put(pagination.searchBy(),"*" + pagination.searchTearm().toLowerCase(Locale.ROOT) + "*"));
        }

        JsonObject queryJson = new JsonObject().put("query", matchJson).put("from", (pagination.page() -1) * pagination.limit()).put("size", pagination.limit());
        request.setJsonEntity(queryJson.encode());
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());

        JsonObject json = new JsonObject(responseBody);
        JsonArray hits = json.getJsonObject("hits").getJsonArray("hits");
        int totalResults = json.getJsonObject("hits").getJsonObject("total").getInteger("value");
        MovieCollection results = new MovieCollection(new ArrayList<>(hits.size()));

        for (int i = 0; i < hits.size(); i++) {
            JsonObject hit = hits.getJsonObject(i);
            MovieResources movie = hit.getJsonObject("_source").mapTo(MovieResources.class);
            results.addMovie(new Movie(
                    new MovieId(movie.id),
                    movie.cover,
                    movie.title,
                    movie.genreName,
                    movie.price,
                    movie.year,
                    movie.rating,
                    movie.description
            ));
        }

        return new Movies(results, pagination, totalResults);
    }

    @Override
    @Transactional
    public Movie getMovieById(MovieId movieId) throws IOException {
        Request request = new Request(
                "GET",
                "/movies/_search");
        JsonObject matchJson = new JsonObject().put("ids", new JsonObject().put("values", new String[]{movieId.id()}));
        JsonObject queryJson = new JsonObject().put("query", matchJson);
        request.setJsonEntity(queryJson.encode());
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());

        JsonObject json = new JsonObject(responseBody);
        JsonArray hits = json.getJsonObject("hits").getJsonArray("hits");

        JsonObject hit = hits.getJsonObject(0);
        MovieResources movieResources = hit.getJsonObject("_source").mapTo(MovieResources.class);

        return new Movie(
                movieId,
                movieResources.cover,
                movieResources.title,
                movieResources.genreName,
                movieResources.price,
                movieResources.year,
                movieResources.rating,
                movieResources.description
        );
    }

    @Override
    public boolean exists(MovieId movieId) throws IOException {
        Request request = new Request(
                "GET",
                "/movies/_search");
        JsonObject matchJson = new JsonObject().put("ids", new JsonObject().put("values", new String[]{movieId.id()}));
        JsonObject queryJson = new JsonObject().put("query", matchJson);
        request.setJsonEntity(queryJson.encode());
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());

        JsonObject json = new JsonObject(responseBody);
        JsonArray hits = json.getJsonObject("hits").getJsonArray("hits");

        return hits.size() > 0 && hits.getJsonObject(0) != null;
    }

}
