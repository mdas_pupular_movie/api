package org.acme.api.movies.movie.Domain.ValueObjects;

import java.util.Objects;

public class Pagination {
    public String searchTearm;
    public int page;
    public String searchBy;
    public int limit;

    public Pagination(String searchTearm, int page, String searchBy, int limit) {
        this.searchTearm = searchTearm;
        this.page = page;
        this.searchBy = searchBy;
        this.limit = limit;
    }

    public String searchTearm() {
        return searchTearm;
    }

    public int page() {
        return page;
    }

    public String searchBy() {
        return searchBy;
    }

    public int limit() {
        return limit;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pagination)) {
            return false;
        }
        Pagination other = (Pagination) o;
        return Objects.equals(searchTearm, other.searchTearm)
                && Objects.equals(searchBy, other.searchBy)
                && Objects.equals(limit, other.limit)
                && Objects.equals(page, other.page);
    }
}
