package org.acme.api.movies.movie.Application.DTO;

public class GetMovieByIdRequest {
    private String id;

    public GetMovieByIdRequest(String id) {
        this.id = id;
    }

    public String movieId() {
        return id;
    }
}
