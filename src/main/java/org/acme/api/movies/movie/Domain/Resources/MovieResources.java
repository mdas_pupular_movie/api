package org.acme.api.movies.movie.Domain.Resources;

import java.util.Date;
import java.util.Objects;

public class MovieResources {
    public String id;
    public String cover;
    public String title;
    public String genreName;
    public float price;
    public Date year;
    public float rating;
    public String description;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieResources)) {
            return false;
        }
        MovieResources other = (MovieResources) o;
        return Objects.equals(id, other.id)
                && Objects.equals(cover, other.cover)
                && Objects.equals(title, other.title)
                && Objects.equals(genreName, other.genreName)
                && price == other.price
                && Objects.equals(year, other.year)
                && rating == other.rating
                && Objects.equals(description, other.description);
    }
}
