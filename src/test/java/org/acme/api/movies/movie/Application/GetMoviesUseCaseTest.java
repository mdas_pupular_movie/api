package org.acme.api.movies.movie.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.movie.Application.DTO.GetMoviesUseCaseRequest;
import org.acme.api.movies.movie.Domain.Converter.MoviesListConverter;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.Resources.ResultMovieResources;
import org.acme.api.movies.movie.Domain.Searcher.MoviesSearcher;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
public class GetMoviesUseCaseTest {
  @Inject
  GetMoviesUseCase useCase;

  @InjectMock
  MoviesSearcher searcher;

  @InjectMock
  MoviesListConverter converter;

  private ResultMovieResources resources;

  @BeforeEach
  void setUp() throws IOException, CustomException {
    Movie movie = new Movie(
            new MovieId("id"),
            "cover",
            "Fantasy Movie",
            "Fantasy",
            2.0F,
            new Date(2022, Calendar.MARCH, 1),
            (float) 1.4,
            "Description fantasy movie"
    );

    MovieCollection collection = new MovieCollection();
    collection.addMovie(movie);

    Pagination pag = new Pagination("", 1, "", 1);

    Movies movies = new Movies(
      collection,
      pag,
      1
    );

    MovieResources resource = new MovieResources();
    resource.id = "id";
    resource.cover = "cover";
    resource.description = "description";
    resource.genreName = "Horror";
    resource.rating = 5.0F;
    resource.price = (float) 0.9;
    resource.title = "title";
    resource.year = new Date(2022, Calendar.APRIL,12);

    ArrayList<MovieResources> list = new ArrayList<>();
    list.add(resource);

    resources = new ResultMovieResources();
    resources.total_results = 1;
    resources.total_pages = 1;
    resources.results = list;
    resources.page = 1;

    when(searcher.execute(any())).thenReturn(movies);

    when(converter.convert(movies)).thenReturn(resources);
  }

  @Test
  public void useCaseGetMoviesTestResponse() throws IOException, CustomException {
    ResultMovieResources result = useCase.run(new GetMoviesUseCaseRequest(new Pagination("", 1, "", 1)));
    Assertions.assertEquals(resources, result);
  }
}
