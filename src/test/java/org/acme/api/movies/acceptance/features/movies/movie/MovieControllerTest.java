package org.acme.api.movies.acceptance.features.movies.movie;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.acme.api.movies.movie.Infraestructure.ControllerRequest.MovieControllerRequest;
import org.acme.api.movies.movie.Infraestructure.Repositories.ElasticMovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class MovieControllerTest {
    @InjectMock
    ElasticMovieRepository repository;

    @BeforeEach
    public void setUp() throws IOException {
        Movie movie = new Movie(
            new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB"),
            "https://image.tmdb.org/t/p/w500/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg",
            "Fantastic Beasts: The Secrets of Dumbledore",
            "Fantasy",
            4.99F,
            new Date(2022, Calendar.MARCH, 1),
            (float) 6.9,
            "Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers."
        );

        MovieCollection collection = new MovieCollection();
        collection.addMovie(movie);

        Pagination pag = new Pagination("", 1, "", 1);

        Movies movies = new Movies(
          collection,
          pag,
          1
        );

        Mockito.when(repository.getMovies(any(Pagination.class))).thenReturn(movies);
        Mockito.when(repository.getMovieById(any())).thenReturn(movie);
        Mockito.when(repository.exists(any())).thenReturn(true);
    }

    @Test
    public void testGetEndpoint() {
        given()
        .pathParam("movieId", "jrgifaYeUtTnaH7NF5Drkgjg2MB")
                .when()
                .get("/movies/{movieId}")
          .then()
             .statusCode(200)
             .body(is("{\"id\":\"jrgifaYeUtTnaH7NF5Drkgjg2MB\",\"cover\":\"https://image.tmdb.org/t/p/w500/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg\",\"title\":\"Fantastic Beasts: The Secrets of Dumbledore\",\"genreName\":\"Fantasy\",\"price\":4.99,\"year\":\"3922-03-01T00:00:00.000+00:00\",\"rating\":6.9,\"description\":\"Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers.\"}"));
    }

    @Test
    public void testPostEndpoint() {
        MovieControllerRequest request = new MovieControllerRequest();
        request.limit = 1;
        request.page = 1;
        request.searchBy = "";
        request.searchTearm = "";

        given()
            .contentType(ContentType.JSON)
            .body(request)
                .when().post("/movies")
                    .then()
                    .statusCode(200)
                        .body(is("{\"page\":1,\"results\":[{\"id\":\"jrgifaYeUtTnaH7NF5Drkgjg2MB\",\"cover\":\"https://image.tmdb.org/t/p/w500/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg\",\"title\":\"Fantastic Beasts: The Secrets of Dumbledore\",\"genreName\":\"Fantasy\",\"price\":4.99,\"year\":\"3922-03-01T00:00:00.000+00:00\",\"rating\":6.9,\"description\":\"Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers.\"}],\"total_pages\":2,\"total_results\":1}"));
    }
}
