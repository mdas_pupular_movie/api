package org.acme.api.movies.acceptance.features.movies.cart;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.*;
import org.acme.api.movies.cart.Infraestructure.ControllerRequest.CartMovieControllerRequest;
import org.acme.api.movies.cart.Infraestructure.Repositories.ElasticMovieCartRepository;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@QuarkusTest
public class CartControllerTest {
    @InjectMock
    InfinispanCartRepository repository;

    @InjectMock
    ElasticMovieCartRepository elasticRepository;

    private Cart cart;

    @BeforeEach
    public void setUp() throws IOException, CustomException {
        cart = new Cart(
            new CartId("29415786-d32d-4066-991f-e335e52345dd"),
            new MovieCollection()
        );

        Mockito.when(repository.getCart(cart.cartId().getId())).thenReturn(cart);
        Mockito.when(repository.exists(any())).thenReturn(true);
    }

    @Test
    public void testGetCartEndpoint() {
        given()
                .pathParam("cartId", "29415786-d32d-4066-991f-e335e52345dd")
                .when().get("/cart/{cartId}")
          .then()
             .statusCode(200)
             .body(is("{\"cartId\":\"29415786-d32d-4066-991f-e335e52345dd\",\"moviesList\":[],\"totalPrice\":0.0}"));
    }

    @Test
    public void testCreateCartEndpoint() {
        given()
            .contentType(ContentType.JSON)
                .when().post("/cart")
                    .then()
                    .statusCode(200);
    }

    @Test
    public void testDeleteCartEndpoint() {
        given()
                .pathParam("cartId", "29415786-d32d-4066-991f-e335e52345dd")
            .when().delete("/cart/{cartId}")
                .then()
                .statusCode(200);
    }

    @Test
    public void testAddMovieEndpoint() throws IOException {

        MovieId movieId = new MovieId("7qop80YfuO0BwJa1uXk1DXUUEwv");

        MovieElastic movieElastic = new MovieElastic(
                movieId.movieId(),
                "The Bad Guys",
                new Price(1.0),
                new Quantity(1)
        );

        ArrayList<MovieId> request = new ArrayList<>();
        request.add(movieId);

        ArrayList<MovieElastic> moviesFound = new ArrayList<>();
        moviesFound.add(movieElastic);

        cart.moviesList().addMovie(new MovieCart(
                movieElastic.movieId(),
                movieElastic.title(),
                movieElastic.quantity()
        ));

        Mockito.when(elasticRepository.getMovieCart(any(request.getClass()))).thenReturn(moviesFound);
        doNothing().when(repository).save(cart);

        CartMovieControllerRequest request1 = new CartMovieControllerRequest();
        request1.cartId = "29415786-d32d-4066-991f-e335e52345dd";
        request1.movieId = "7qop80YfuO0BwJa1uXk1DXUUEwv";

        given()
            .contentType(ContentType.JSON).body(request1)
                .when().post("/cart/addMovie")
                    .then()
                    .statusCode(200);
    }

    @Test
    public void testDeleteMovieEndpoint() throws IOException {
        MovieId movieId = new MovieId("7qop80YfuO0BwJa1uXk1DXUUEwv");

        MovieElastic movieElastic = new MovieElastic(
                movieId.movieId(),
                "The Bad Guys",
                new Price(1.0),
                new Quantity(1)
        );

        ArrayList<MovieId> request = new ArrayList<>();
        request.add(movieId);

        ArrayList<MovieElastic> moviesFound = new ArrayList<>();
        moviesFound.add(movieElastic);

        cart.moviesList().addMovie(new MovieCart(
                movieElastic.movieId(),
                movieElastic.title(),
                movieElastic.quantity()
        ));
        Mockito.when(elasticRepository.getMovieCart(any(request.getClass()))).thenReturn(moviesFound);

        given().body("{\"cartId\":\"29415786-d32d-4066-991f-e335e52345dd\",\"movieId\":\"7qop80YfuO0BwJa1uXk1DXUUEwv\"}")
                .contentType(ContentType.JSON)
                .when().delete("/cart/removeMovie")
                .then()
                .statusCode(200);
    }
}
