package org.acme.api.movies.cart.Infraestructure.Repositories;

import io.quarkus.test.junit.QuarkusTest;

import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

@QuarkusTest
public class ElasticMovieCartRepositoryTest {
    /*
      @Inject
      ElasticMovieCartRepository repository;
    */

    private MovieCart movie;
    private ArrayList<MovieElastic> movies;

    @BeforeEach
    public void setUp(){
        movie = new MovieCart(
                "jrgifaYeUtTnaH7NF5Drkgjg2MB",
                "Fantastic Beasts: The Secrets of Dumbledore",
                new Quantity(1)
        );
        MovieCollection collection = new MovieCollection();
        collection.addMovie(movie);
        movies = new ArrayList<>();
    }

    @Test
    public void testGetMovies(){
        /* TO DO: Testing to connect with the database
          ArrayList<MovieId> moviesIds = new ArrayList<>();
          moviesIds.add(new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB"));
        */

        movies.add(new MovieElastic(
                movie.movieId(),
                movie.title(),
                new Price(4.99),
                new Quantity(1)
        ));

        //ArrayList<MovieElastic> result = repository.getMovieCart(moviesIds);

        ArrayList<MovieElastic> result = movies;

        Assertions.assertEquals(movies, result);
    }
}
