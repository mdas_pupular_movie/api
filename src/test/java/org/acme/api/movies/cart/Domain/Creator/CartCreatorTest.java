package org.acme.api.movies.cart.Domain.Creator;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.mockito.Mockito.*;

@QuarkusTest
public class CartCreatorTest {

    @InjectMock
    InfinispanCartRepository repository;

    @Inject
    CartCreator creator;

    @Test
    public void createCart() {

        CartId cartId = new CartId("1");

        doNothing().when(repository).save(any());

        creator.create(cartId);

        verify(repository, times(1)).save(any());

    }


}
