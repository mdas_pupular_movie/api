package org.acme.api.movies.cart.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Remove.CartRemover;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@QuarkusTest
public class RemoveCartUseCaseTest {
    @Inject
    RemoveCartUseCase useCase;

    @InjectMock
    CartRemover remover;

    @InjectMock
    InfinispanCartRepository repository;

    private CartId id;

    @BeforeEach
    public void setUp(){
        id = new CartId();
    }

    @Test
    public void useCaseTestRemoveCart() throws CustomException {
        useCase.execute(id);
        verify(remover, times(1)).remove(id);
    }

    @Test
    public void finderTestResponseFail() throws CustomException {
        when(repository.exists(any())).thenReturn(false);
        doThrow(CustomException.class).when(remover).remove(any());
        Assertions.assertThrowsExactly(CustomException.class, () -> useCase.execute(id));
    }
}
