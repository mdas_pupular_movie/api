package org.acme.api.movies.cart.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Converter.CartConverter;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Finder.CartFinder;
import org.acme.api.movies.cart.Domain.Resources.CartResponseResource;
import org.acme.api.movies.cart.Domain.Searcher.MovieCartSearcher;
import org.acme.api.movies.cart.Domain.ValueObjects.*;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@QuarkusTest
public class GetCartUseCaseTest {
    @InjectMock
    CartFinder finder;

    @InjectMock
    MovieCartSearcher searcher;

    @InjectMock
    CartConverter converter;

    @Inject
    GetCartUseCase useCase;

    private Cart cart;
    private CartId cartId;
    private CartResponseResource response;

    @BeforeEach
    void setUp(){
        cartId = new CartId();

        cart = new Cart(
                cartId,
                new MovieCollection()
        );

        response = new CartResponseResource();
        response.cartId = cartId.getId();
        response.totalPrice = 0.0;
        response.moviesList = new ArrayList<>();
    }

    @Test
    public void useCaseTestResponse() throws IOException, CustomException {
        Mockito.when(finder.find(any())).thenReturn(cart);
        Mockito.when(searcher.search(any())).thenReturn(new ArrayList<>());
        Mockito.when(converter.convert(any(Cart.class), any())).thenReturn(response);
        CartResponseResource result = useCase.execute(cartId.getId());
        Assertions.assertEquals(response, result);
    }

    @Test
    public void useCaseTestResponseError() throws CustomException {
        doThrow(CustomException.class).when(finder).find(any());
        Assertions.assertThrowsExactly(CustomException.class, () -> useCase.execute("cartId"));
    }
}
